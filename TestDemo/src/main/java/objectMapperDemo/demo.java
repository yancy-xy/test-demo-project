package objectMapperDemo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationConfig;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class demo {

    public static void main(String[] args) throws IOException {
        User user = new User();

        user.setName("菠萝大象");

        user.setGender(Gender.MALE);

        List<Account> accounts = new ArrayList<Account>();

        Account account = new Account();

        account.setId(1);

        account.setBalance(BigDecimal.valueOf(1900.2));

        account.setCardId("423335533434");

        account.setDate(new Date());

        accounts.add(account);

        account = new Account();

        account.setId(2);

        account.setBalance(BigDecimal.valueOf(5000));

        account.setCardId("625444548433");

        account.setDate(new Date());
        List<String> strings = new ArrayList<>();
        strings.add("111");
        strings.add("222");
        account.setStrings(strings);

        accounts.add(account);

        user.setAccounts(accounts);

        ObjectMapper mapper = new ObjectMapper();

        //mapper.configure(SerializationConfig.Feature.INDENT_OUTPUT, Boolean.TRUE);

        String json = mapper.writeValueAsString(user);

        System.out.println("Java2Json: "+json);

        user = mapper.readValue(json, User.class);

        System.out.println("Json2Java: "+mapper.writeValueAsString(user));
    }


}
