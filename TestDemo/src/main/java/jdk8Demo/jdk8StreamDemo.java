package jdk8Demo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class jdk8StreamDemo {
    public static void main(String[] args) {
        //测试数据，请不要纠结数据的严谨性
        List<StudentInfo> studentList = new ArrayList<>();
        studentList.add(new StudentInfo("李小明", true, 18, 1.76, LocalDate.of(2001, 3, 23)));
        studentList.add(new StudentInfo("张小丽", false, 18, 1.61, LocalDate.of(2001, 6, 3)));
        studentList.add(new StudentInfo("王大朋", true, 19, 1.82, LocalDate.of(2000, 3, 11)));
        studentList.add(new StudentInfo("陈小跑", false, 17, 1.67, LocalDate.of(2002, 10, 18)));

        //输出List
        StudentInfo.printStudents(studentList);

        //按年龄排序(Integer类型)
        List<StudentInfo> studentsSortName = studentList.stream().sorted(Comparator.comparing(StudentInfo::getAge).reversed()).collect(Collectors.toList());
        //排序后输出
        StudentInfo.printStudents(studentsSortName);

        //从对象列表中提取一列(以name为例)
        List<String> nameList = studentList.stream().map(StudentInfo::getName).collect(Collectors.toList());
        //提取后输出name
        nameList.forEach(s -> System.out.println(s));

        //从对象列表中提取age并排重
        List<Integer> ageList = studentList.stream().map(StudentInfo::getAge).distinct().collect(Collectors.toList());
        ageList.forEach(a -> System.out.println(a));

        //过滤年龄小于18的同学
        List<StudentInfo> checkAgeStrudents = studentList.stream().filter(item -> item.getAge() >= 18 && item.getHeight() < 1.8).collect(Collectors.toList());
        StudentInfo.printStudents(checkAgeStrudents);

    }
}
