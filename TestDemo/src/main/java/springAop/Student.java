package springAop;

import java.util.ArrayList;
import java.util.Objects;

public class Student {
    private int age;
    private String name;
    private int studentId;
    private int classId;

    public Student() {
    }

    @Override
    public boolean equals(Object o) {
        //Collection
        ArrayList list = new ArrayList<>();
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return age == student.age &&
                studentId == student.studentId &&
                Objects.equals(name, student.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(age, name, studentId);
    }
}
