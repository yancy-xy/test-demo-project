package springAop;

public interface Person {
    String sayHello(String name);
}
