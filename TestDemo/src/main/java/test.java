import lombok.Data;
import lombok.ToString;

import java.text.SimpleDateFormat;

public class test {
    private static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

    @Data
    @ToString
    static
    class Order {
        Product product;
        String orderId;
    }

    @Data
    @ToString
    static
    class Product {
        private String productName;
        private String skuId;
        private String skuPrice;
    }

    public static void main(String[] args) {
        Product prod1 = new Product();
        Order order1 = new Order();
        order1.setProduct(prod1);
        order1.setOrderId("order1");
        prod1.setProductName("prod1");
        prod1.setSkuId("1");
        prod1.setSkuPrice("1.00");
        checkOrder(order1);
        Product prod2 = order1.getProduct();
        updProduct(prod2, "3.00");
        System.out.println("no.3->" + order1.toString());
    }

    public static boolean checkOrder(Order order1) {
        System.out.println("no.1->" + order1.toString());
        Product product = order1.getProduct();
        updProduct(product, "2.00");
        System.out.println("no.2->" + order1.toString());
        return true;
    }

    public static boolean updProduct(Product product, String price) {
        product.setSkuPrice(price);
        System.out.println("product - >" + product.toString());
        return true;
    }
}
