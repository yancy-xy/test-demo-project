package singletonPractical;

public class SingletonLazy1 {
    private static SingletonLazy1 singleton = null;
    private SingletonLazy1(){};
    public static synchronized SingletonLazy1 getInstance(){
        if(singleton == null){
            return new SingletonLazy1();
        }
        return singleton;
    }
}
