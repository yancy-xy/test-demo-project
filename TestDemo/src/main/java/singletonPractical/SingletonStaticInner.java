package singletonPractical;

//静态内部类（线程安全）
//这种方式兼顾了延迟初始化，线程安全，是一种比较推荐的写法。
public class SingletonStaticInner {
    private static SingletonStaticInner singleton;
    private SingletonStaticInner(){}

    private static class StaticHolder{
        private static  final SingletonStaticInner instance = new SingletonStaticInner();
    }
    public static SingletonStaticInner getInstance(){
        return StaticHolder.instance;
    }
}
