package singletonPractical;
//变种3专门针对变种2，可谓DCL 2.0。
//针对变种3的“半个对象”问题，变种3在instance上增加了volatile关键字，原理见上述参考。
public class SingletonLazy3 {
    private static volatile SingletonLazy3 singleton = null;
    private SingletonLazy3(){}
    public static synchronized SingletonLazy3 getInstance(){
        if(singleton == null){
            synchronized (SingletonLazy3.class){
                if(singleton == null) return new SingletonLazy3();
            }
        }
        return singleton;
    }
}
