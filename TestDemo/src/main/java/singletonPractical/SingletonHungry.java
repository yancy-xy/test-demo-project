package singletonPractical;

//饿汉式-线程安全
//值得注意的时，单线程环境下，饿汉与饱汉在性能上没什么差别；但多线程环境下，由于饱汉需要加锁，饿汉的性能反而更优。
public class SingletonHungry {
    private static final SingletonHungry singleton= new SingletonHungry();
    private SingletonHungry() {

    }
    public static SingletonHungry getInstance(){
        return singleton;
    }

}


