package singletonPractical;
//变种2是“臭名昭著”的DCL 1.0。
//针对变种1中单例初始化后锁仍然无法避开的问题，变种2在变种1的外层又套了一层check，
//加上synchronized内层的check，即所谓“双重检查锁”（Double Check Lock，简称DCL）。
public class SingletonLazy2 {
    private static SingletonLazy2 singleton = null;
    SingletonLazy2(){}
    public static synchronized SingletonLazy2 getInstance(){
        if(singleton == null){
            synchronized (SingletonLazy2.class){
                if(singleton == null) return new SingletonLazy2();
            }
        }
            return singleton;
    }
}
