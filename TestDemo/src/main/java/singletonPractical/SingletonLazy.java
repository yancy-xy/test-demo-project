package singletonPractical;

//懒汉-先不初始化单例，等第一次使用的时候再初始化，即“懒加载”
//懒汉-基础型
//单线程环境下，基础饱汉是猴哥最喜欢的写法。
//但多线程环境下，基础饱汉就彻底不可用了。下面的几种变种都在试图解决基础饱汉线程不安全的问题。
public class SingletonLazy {
    private  static  SingletonLazy  singleton = null;

    private SingletonLazy(){}

    public static SingletonLazy getInstance(){
        if(singleton == null) {
            singleton = new SingletonLazy();
        }
        return singleton;
    }

}
