package threadDemo;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Demo1 {
    public static volatile int i = 10;

    public static void main(String[] args) {
        /*new Thread() {
            public void run() {
                for(int i=0;i<5;i++) {
                    System.out.println("aaaaaaa");
                }
            }
        }.start();
        new Thread(new Runnable() {
            public void run() {
                for(int i=0;i<5;i++) {
                    System.out.println("bbbbbbb");
                }
            }
        }).start();*/
        new Thread(() -> System.out.println("---" + "aaa")).start();
        new Thread(() -> System.out.println("---" + "bbb")).start();
        Runnable run = new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep((long) ( 1000*Math.random()));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("");
            }
        };

    }
}

