package threadDemo.productAndCosumerDemo;

import java.util.Objects;

public class ProAndConDto {
    private double number;

    public ProAndConDto(double number) {
        this.number = number;
    }

    public double getNumber() {
        return number;
    }

    public void setNumber(double number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProAndConDto that = (ProAndConDto) o;
        return Double.compare(that.number, number) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    @Override
    public String toString() {
        return "ProAndConDto{" +
                "number=" + number +
                '}';
    }
}
