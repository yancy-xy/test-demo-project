package threadDemo.productAndCosumerDemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;

public class Producer implements RunnableFuture {
    private static final Logger logger = LoggerFactory.getLogger(Producer.class);

    private BlockingQueue<ProAndConDto> blockingQueue;


    public Producer(BlockingQueue<ProAndConDto> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }


    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            blockingQueue.add(new ProAndConDto(Math.random()));//待消费者处理
        }
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        return false;
    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    @Override
    public boolean isDone() {
        return false;
    }

    @Override
    public Object get() throws InterruptedException, ExecutionException {
        return null;
    }

    @Override
    public Object get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        return null;
    }
}
