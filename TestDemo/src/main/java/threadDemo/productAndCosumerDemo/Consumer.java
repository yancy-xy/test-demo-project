package threadDemo.productAndCosumerDemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;

public class Consumer implements RunnableFuture {
    private static final Logger logger = LoggerFactory.getLogger(Consumer.class);
    private BlockingQueue<ProAndConDto> blockingQueue;

    public Consumer(BlockingQueue<ProAndConDto> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        try {
            for (; ; ) {
                ProAndConDto temp = blockingQueue.take();
                logger.info(" number is :" + temp.getNumber());
                if (temp.getNumber() == -1) {
                    logger.info("the job is end");
                    break;
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        return false;
    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    @Override
    public boolean isDone() {
        return false;
    }

    @Override
    public Object get() throws InterruptedException, ExecutionException {
        return null;
    }

    @Override
    public Object get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        return null;
    }
}
