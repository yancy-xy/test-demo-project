package threadDemo.blockQueueDemo;

import org.springframework.util.StopWatch;

import java.util.UUID;

public class ArrayQueueDemo {
    public static void main(String[] args) {
        StopWatch stopWatch = new StopWatch(UUID.randomUUID().toString());
        stopWatch.start("阻塞队列Demo");
        ArrayQueue<String> queue = new ArrayQueue<>(3);
        queue.put("1");
        queue.put("12");
        queue.put("123");
        while (!queue.isEmpty()) {
            System.out.println(queue.get());
        }
        stopWatch.stop();
        System.out.println(stopWatch.prettyPrint());
    }
}
