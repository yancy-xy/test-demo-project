package threadDemo;

public class ThreadLocalDemo {
    //这里为什么使用static关键字进行修饰，自己去想，不解释。
    private static ThreadLocal<String> sThreadLocal = new ThreadLocal<>();
    private static ThreadLocal<String> sThreadLocal2 = new ThreadLocal<>();
    public static void main(String args[]) {
        //主线程
        sThreadLocal.set("这是在主线程中");
        System.out.println("线程名字：" + Thread.currentThread().getName() + "---" + sThreadLocal.get());
        sThreadLocal2.set("111");
        //线程a
        new Thread(new Runnable() {
            public void run() {
                sThreadLocal.set("这是在线程a中");
                System.out.println("线程名字：" + Thread.currentThread().getName() + "---" + sThreadLocal.get());
            }
        }, "线程a").start();
        //线程b
        new Thread(new Runnable() {
            public void run() {
                sThreadLocal.set("这是在线程b中");
                System.out.println("线程名字：" + Thread.currentThread().getName() + "---" + sThreadLocal.get());
            }
        }, "线程b").start();
        //线程c  这个线程是使Lambda建立的，跟上面两个是一样的。不会使用Lambda表达式的同学请看我的另一篇博客
        new Thread(() -> {
            sThreadLocal.set("这是在线程c中");
            System.out.println("线程名字：" + Thread.currentThread().getName() + "---" + sThreadLocal.get());
        }, "线程c").start();


        System.out.println(sThreadLocal.get());;
        System.out.println(sThreadLocal2.get());;
    }

}
