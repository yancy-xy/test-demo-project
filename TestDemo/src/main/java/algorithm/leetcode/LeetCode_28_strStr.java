package algorithm.leetcode;

public class LeetCode_28_strStr {
    public int strStr(String haystack, String needle) {
        if (needle.length() > haystack.length()) {
            return -1;
        }
        if (needle.length() == 0) {
            return 0;
        }
        return haystack.indexOf(needle);
    }

}
