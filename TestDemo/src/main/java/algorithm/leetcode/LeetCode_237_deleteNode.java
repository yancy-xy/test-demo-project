package algorithm.leetcode;

public class LeetCode_237_deleteNode {
    public class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }

    //神仙题目 的神仙理解：
    //这道题细思极恐：如何让自己在世界上消失，但又不死？ —— 将自己完全变成另一个人，再杀了那个人就行了。
    public void deleteNode(ListNode node) {
        ListNode next = node.next;
        node.next = node.next.next;
        node.val = next.val;
    }
}
