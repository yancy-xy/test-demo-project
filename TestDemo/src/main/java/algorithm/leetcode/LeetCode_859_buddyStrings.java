package algorithm.leetcode;

public class LeetCode_859_buddyStrings {
    public boolean buddyStrings(String s, String goal) {
        int m = s.length(), n = goal.length(), diff = 0;
        if (m != n) return false;
        int[] hash1 = new int[26], hash2 = new int[26];
        char[] ss = s.toCharArray(), goals = goal.toCharArray();
        for (int i = 0; i < n; i++) {
            hash1[ss[i] - 'a']++;
            hash2[goals[i] - 'a']++;
            if (ss[i] != goals[i]) diff++;
        }
        boolean ans = false;
        for (int i = 0; i < 26; i++) {
            if (hash1[i] != hash2[i]) return false;
            if (hash1[i] > 1) ans = true;
        }
        return diff == 2 || (diff == 0 && ans);

    }
}
