package algorithm.leetcode;

public class LeetCode_62_uniquePaths {
    public int uniquePaths(int m, int n) {
        int[][] dp = new int[m][n];
        for (int i = m - 1; i >= 0; i--) {
            for (int j = n - 1; j >= 0; j--) {
                if (i == m - 1 || j == n - 1) {
                    dp[i][j] = 1;
                }else{
                    dp[i][j] = dp[i][j + 1] + dp[i + 1][j];
                }
            }
        }
        return dp[0][0];
    }

    public static void main(String[] args) {
        LeetCode_62_uniquePaths leetCode_62_uniquePaths = new LeetCode_62_uniquePaths();
        int res = leetCode_62_uniquePaths.uniquePaths(3, 3);
        System.out.println(res);
    }
}
