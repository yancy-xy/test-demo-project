package algorithm.leetcode;

import java.util.Arrays;

public class LeetCode_1128_numEquivDominoPairs {
    public int numEquivDominoPairs(int[][] dominoes) {
        int[] hash = new int[100];
        int res = 0;
        for (int[] item :dominoes){
            Arrays.sort(item);
            int index = item[0] *10 +item[1];
            res += hash[index];
            hash[index] +=1;
        }
        return res;
    }


    public static void main(String[] args) {
        LeetCode_1128_numEquivDominoPairs leetCode_1128_numEquivDominoPairs = new LeetCode_1128_numEquivDominoPairs();
        int[][] req = new int[][]{{1,2},{2,1},{3,4},{5,6}};
        leetCode_1128_numEquivDominoPairs.numEquivDominoPairs(req);

    }
}
