package algorithm.leetcode;

public class LeetCode_941_validMountainArray {
    public boolean validMountainArray(int[] A) {
        int len = A.length;
        int left = 0;
        int right = A.length - 1;
        while (left < len - 1 && A[left] < A[left + 1]) {
            left += 1;
        }
        while (right > 0 && A[right] < A[right - 1]) {
            right -= 1;
        }
        return left == right && left != 0 && right != len - 1;
    }

    public static void main(String[] args) {
        LeetCode_941_validMountainArray solution = new LeetCode_941_validMountainArray();
        int[] Ademo = {2, 1};
        boolean res = solution.validMountainArray(Ademo);
        System.out.println(res);
    }
}
