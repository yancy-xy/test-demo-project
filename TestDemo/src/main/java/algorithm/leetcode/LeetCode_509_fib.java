package algorithm.leetcode;

public class LeetCode_509_fib {
    public int fib(int n) {
        if (n == 0) return 0;
        if (n == 1) return 1;
        int[] dp = new int[n + 1];
        dp[0] = 0;
        dp[1] = 1;
        for (int i = 2; i <= n; i++) {
            dp[i] = dp[i - 1] + dp[i - 2];
        }
        return dp[n];
    }

    public static void main(String[] args) {
        LeetCode_509_fib leetCode_509_fib = new LeetCode_509_fib();
        int res = leetCode_509_fib.fib(4);
        System.out.println("res ->" + res);
    }
}
