package algorithm.leetcode;

import java.util.Arrays;

public class LeetCode_1030_allCellsDistOrder {
    public int[][] allCellsDistOrder(int R, int C, int r0, int c0) {
        int[][] res = new int[R * C][2];
        int index = 0;
        for (int i = 0; i < R; i++)
            for (int j = 0; j < C; j++) {
                int[] xy = {i, j};
                res[index++] = xy;
            }
        Arrays.sort(res, (int[] o1, int[] o2) -> {
            int dis1 = Math.abs(o1[0] - r0) + Math.abs(o1[1] - c0);
            int dis2 = Math.abs(o2[0] - r0) + Math.abs(o2[1] - c0);
            return dis1 - dis2;
        });
        return res;
    }

    public static void main(String[] args) {
        int R = 1, C = 2, r0 = 0, c0 = 0;
        LeetCode_1030_allCellsDistOrder leetCode_1030_allCellsDistOrder = new LeetCode_1030_allCellsDistOrder();
        int[][] res = leetCode_1030_allCellsDistOrder.allCellsDistOrder(R, C, r0, c0);
        for (int[] item : res) {
            System.out.print("(" + item[0] + "," + item[1] + ") ");
        }
    }
}
