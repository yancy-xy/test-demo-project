package algorithm.leetcode;

public class LeetCode_1122_relativeSortArray {
    public int[] relativeSortArray(int[] arr1, int[] arr2) {
        int[] res = new int[arr1.length];
        int[] bucket = new int[1001];//放入所有arr1的信息,结构->bucket[num , 出现次数]
        for (int i = 0; i < arr1.length; i++) {
            int num = arr1[i];
            bucket[num]++;
        }
        int index = 0;
        for (int i = 0; i < arr2.length; i++) {
            while (bucket[arr2[i]] != 0) {
                res[index] = arr2[i];
                index += 1;
                bucket[arr2[i]]--;
            }
        }
        for (int i = 0; i < bucket.length; i++) {
            while (bucket[i] != 0) {
                res[index] = i;
                index += 1;
                bucket[i]--;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        LeetCode_1122_relativeSortArray leetCode_1122_relativeSortArray = new LeetCode_1122_relativeSortArray();
        int[] arr1 = {2, 3, 1, 3, 2, 4, 6, 7, 9, 2, 19};
        int[] arr2 = {2, 1, 4, 3, 9, 6};
        int[] res = leetCode_1122_relativeSortArray.relativeSortArray(arr1, arr2);
        for (int item : res) {
            System.out.print(item + ",");
        }
    }
}
