package algorithm.leetcode;

import java.util.ArrayList;
import java.util.List;

public class LeetCode_500_findWords {
    public String[] findWords(String[] words) {
        List<String> res = new ArrayList<>();
        String row1= "qwertyuiopQWERTYUIOP";
        String row2 = "asdfghjklASDFGHJKL";
        String row3 = "zxcvbnmZXCVBNM";
        for (String word : words) {
            int n1 = 0, n2 = 0 , n3 = 0 ,leng = word.length();
            for(int i = 0 ; i < leng ; i++){
                if(row1.contains(word.charAt(i)+"")) n1++;
                else if(row2.contains(word.charAt(i)+"")) n2++;
                else  n3++;
            }
            if(n1 == leng || n2 == leng || n3 == leng) {
                res.add(word);
            }
        }
        return res.toArray(new String[res.size()]);
    }
}
