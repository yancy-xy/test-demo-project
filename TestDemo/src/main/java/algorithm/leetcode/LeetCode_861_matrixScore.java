package algorithm.leetcode;

public class LeetCode_861_matrixScore {
    public int matrixScore(int[][] A) {
        // 思路：贪心
        int res = 0;
        int m = A.length;
        int n = A[0].length;
        // 1. 进行行翻转，可以将第一列全部翻转成 1
        res += m * (1 << (n - 1));

        // 2. 接下对每一列进行分析，让每一列的 1 最多
        for (int j = 1; j < n; j++) {
            int count1 = 0;
            for (int i = 0; i < m; i++) {
                count1 += A[i][0] == 1 ? A[i][j] : 1 - A[i][j];
            }
            res += Math.max(count1, m - count1) * (1 << (n - 1 - j));
        }
        return res;
    }

    public static void main(String[] args) {

    }
}
