package algorithm.leetcode;

import java.util.Arrays;

public class LeetCode_213_rob {
    public static int rob(int[] nums) {
        if (nums.length == 0) return 0;
        if (nums.length == 1) return nums[0];
        int length = nums.length;
        int[] nums2 = Arrays.copyOfRange(nums, 1, length + 1);
//        int[] nums2 = new int[length];
//        System.arraycopy(nums, 1, nums2, 0, length - 1);
        int robNum1 = dpRob(nums, 0, length - 1);
        int robNum2 = dpRob(nums2, 0, length - 1);
        return Math.max(robNum1, robNum2);
    }

    private static int dpRob(int[] nums, int start, int end) {
        int length = nums.length;
        int[] dp = new int[length + 1];
        dp[0] = 0;
        dp[1] = nums[0];
        for (int i = 2; i <= end; i++) {
            dp[i] = Math.max(dp[i - 1], dp[i - 2] + nums[i - 1]);
        }
        return dp[end];
    }

    public static void main(String[] args) {
        int[] nums = {2, 3, 2};
        rob(nums);
    }
}
