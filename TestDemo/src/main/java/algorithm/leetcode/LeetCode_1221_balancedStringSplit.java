package algorithm.leetcode;

public class LeetCode_1221_balancedStringSplit {
    public int balancedStringSplit(String s) {
        /*int res = 0;
        int len = s.length();
        String resString = s.replaceAll("L", "-1,").replaceAll("R", "1,");
        String[] numsArray = resString.split(",");
        int temp = 0;
        for (int i = 0; i < len; i++) {
            temp += Integer.valueOf(numsArray[i]);
            if (temp == 0) {
                res += 1;
            }
        }
        return res;*/
        int res = 0;
        int len = s.length();
        int temp = 0;
        for (int i = 0; i < len; i++) {
            if ((s.charAt(i) == 'R')) {
                temp += 1;
            }
            if ((s.charAt(i) == 'L')) {
                temp -= 1;
            }
            if (temp == 0) {
                res += 1;
            }
        }
        return res;
    }
}

