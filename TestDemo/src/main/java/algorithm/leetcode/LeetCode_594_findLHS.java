package algorithm.leetcode;

import java.util.HashMap;
import java.util.Map;

public class LeetCode_594_findLHS {
    public int findLHS(int[] nums) {
        Map<Integer, Integer> count = new HashMap<>();
        int ans = 0;
        for (int num : nums) count.put(num, count.getOrDefault(num, 0) + 1);
        for (int k : count.keySet()) {
            int x = count.get(k);
            if (count.containsKey(k + 1)) ans = Math.max(ans, x + count.get(k + 1));
        }
        return ans;
    }
}
