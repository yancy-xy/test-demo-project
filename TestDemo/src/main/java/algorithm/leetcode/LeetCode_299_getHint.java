package algorithm.leetcode;

public class LeetCode_299_getHint {
    //神仙想法:
    public static String getHint(String secret, String guess) {
        int[] nums = new int[10];
        int countA = 0, countB = 0;
        for (int i = 0; i < secret.length(); i++) {
            if (secret.charAt(i) == guess.charAt(i)) countA++;
            else {
                if (nums[guess.charAt(i) - '0']-- > 0) countB++;
                if (nums[secret.charAt(i) - '0']++ < 0) countB++;
            }
        }
        return countA + "A" + countB + "B";
    }
    //常规想法:
    /*public static String getHint(String secret, String guess) {
        int x = 0;
        int y = 0;
        HashMap<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < secret.length(); i++) {
            if (secret.charAt(i) == guess.charAt(i)) x++;
            char c = secret.charAt(i);
            if (!map.containsKey(c)) map.put(c, 1);
            else map.put(c, map.get(c) + 1);
        }
        for (int i = 0; i < secret.length(); i++) {
            char c1 = guess.charAt(i);
            if (map.containsKey(c1) && map.get(c1) > 0) {
                y++;
                map.put(c1, map.get(c1) - 1);
            }
        }
        y = y - x;
        return x + "A" + y + "B";
    }*/

    public static void main(String[] args) {

        String hint = getHint("1122", "1222");
        System.out.println(hint);
    }
}
