package algorithm.leetcode;

public class LeetCode_402_removeKdigits {
    public String removeKdigits(String num, int k) {
        if (k == num.length()) return "0";
        StringBuilder sb = new StringBuilder(num);
        for (int i = 0; i < k; i++) {
            int index = 0;
            for (int j = 1; j < sb.length() && sb.charAt(j) >= sb.charAt(j - 1); j++) {
                index = j;
            }
            sb.delete(index, index + 1);
            while (sb.length() > 1 && '0' == sb.charAt(0)) {
                sb.delete(0, 1);
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        String num = "10200";
        int k = 1;
        LeetCode_402_removeKdigits leetCode_402_removeKdigits = new LeetCode_402_removeKdigits();
        String res = leetCode_402_removeKdigits.removeKdigits(num, k);
        System.out.println(res);
    }
}
