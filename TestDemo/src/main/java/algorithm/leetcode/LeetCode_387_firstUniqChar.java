package algorithm.leetcode;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class LeetCode_387_firstUniqChar {
    public int firstUniqChar(String s) {
        char[] sCharArray = s.toCharArray();
        int[] charMap = new int[26];
        Arrays.fill(charMap, 0);
        Set set = new HashSet<>();
        for (int i = 0; i < sCharArray.length; i++) {
            charMap[sCharArray[i] - 'a'] += 1;
        }
        for (int i = 0; i < sCharArray.length; i++) {
            if (charMap[sCharArray[i] - 'a'] == 1) {
                return i;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        LeetCode_387_firstUniqChar leetCode_387_firstUniqChar = new LeetCode_387_firstUniqChar();
        String input = "loveleetcode";
        int res = leetCode_387_firstUniqChar.firstUniqChar(input);
        System.out.println(res);
    }
}
