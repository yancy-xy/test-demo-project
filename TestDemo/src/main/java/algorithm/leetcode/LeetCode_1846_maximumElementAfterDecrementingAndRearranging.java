package algorithm.leetcode;

import java.util.Arrays;

public class LeetCode_1846_maximumElementAfterDecrementingAndRearranging {
    public static int maximumElementAfterDecrementingAndRearranging(int[] arr) {
        Arrays.sort(arr);
        arr[0] = 1;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] - arr[i - 1] > 1) {
                arr[i] = arr[i - 1] + 1;
            }
        }
        return arr[arr.length - 1];
    }

    public static void main(String[] args) {
        int[] arr = {100,1,1000};
        int res = maximumElementAfterDecrementingAndRearranging(arr);
        System.out.println(res);
    }
}
