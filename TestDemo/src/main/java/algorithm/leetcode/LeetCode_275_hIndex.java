package algorithm.leetcode;

import java.util.Arrays;

public class LeetCode_275_hIndex {
    public int hIndex(int[] citations) {
        int res = 0;
        Arrays.sort(citations);
        for (int i = 0; i < citations.length; i++) {
            int hindex = citations.length - i;
            if (hindex <= citations[i]) {
                return hindex;
            }
        }
        return res;
    }
}
