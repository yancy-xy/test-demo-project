package algorithm.leetcode;

public class LeetCode_83_deleteDuplicates {

    public static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

    public static ListNode deleteDuplicates(ListNode head) {
        if (head == null) {
            return head;
        }
        ListNode ptr = head;
        while (ptr.next != null) {
            if (ptr.val == ptr.next.val) {
                ptr.next = ptr.next.next;
            } else {
                ptr = ptr.next;
            }
        }
        return head;

    }

    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        ListNode temp1 = new ListNode(1);
        ListNode temp2 = new ListNode(2);
        head.next = temp1;
        temp1.next = temp2;
        deleteDuplicates(head);
    }
}
