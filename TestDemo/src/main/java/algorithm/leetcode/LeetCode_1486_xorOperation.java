package algorithm.leetcode;

public class LeetCode_1486_xorOperation {
    public int xorOperation(int n, int start) {
        int[] nums = new int[n];
        int res = 0;
        for (int i = 0; i < n; i++) {
            nums[i] = start + 2 * i;
            res = res ^ nums[i];
        }
        return res;
    }
}
