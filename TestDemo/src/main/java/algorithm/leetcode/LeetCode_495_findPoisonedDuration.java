package algorithm.leetcode;

public class LeetCode_495_findPoisonedDuration {
    public int findPoisonedDuration(int[] timeSeries, int duration) {
        int res = 0;
        for (int i = 0; i < timeSeries.length - 1; i++) {
            if (timeSeries[i + 1] - timeSeries[i] > duration) {
                res += duration;
            } else {
                res += timeSeries[i + 1] - timeSeries[i];
            }
        }
        res += duration;
        return res;
    }
}
