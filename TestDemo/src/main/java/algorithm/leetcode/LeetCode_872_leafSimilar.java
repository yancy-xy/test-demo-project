package algorithm.leetcode;

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode() {
    }

    TreeNode(int val) {
        this.val = val;
    }

    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

public class LeetCode_872_leafSimilar {
    public boolean leafSimilar(TreeNode root1, TreeNode root2) {
        String tree1 = help(root1, "");
        String tree2 = help(root2, "");
        return tree1.equals(tree2);
    }

    public String help(TreeNode root, String str) {
        if (root == null) return str;
        if (root.left == null && root.right == null) {
            str += root.val;
            return str;
        }
        return help(root.left, str) + help(root.right, str);
    }

    public static void main(String[] args) {
        
    }
}
