package algorithm.leetcode;

public class LeetCode_168_convertToTitle {
    public static String convertToTitle(int columnNumber) {
        StringBuilder res = new StringBuilder();
        while (columnNumber > 0) {
            columnNumber--;
            res.append((char) (columnNumber % 26 + 'A'));
            columnNumber =columnNumber / 26;
        }
        return res.reverse().toString();
    }

    public static void main(String[] args) {
        int columnNum = 52;
        String res = convertToTitle(columnNum);
        System.out.println(res);
    }
}
