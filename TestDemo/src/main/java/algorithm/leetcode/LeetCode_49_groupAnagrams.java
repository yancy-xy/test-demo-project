
package algorithm.leetcode;

import java.util.*;

public class LeetCode_49_groupAnagrams {
    public List<List<String>> groupAnagrams(String[] strs) {
        Map<String, List<String>> map = new HashMap<>();
        for (String str : strs) {
            char[] charArray = str.toCharArray();
            Arrays.sort(charArray);
            String word = String.valueOf(charArray);
            if (!map.containsKey(word)) {
                map.put(word, new ArrayList<>());
            }
            map.get(word).add(str);
        }
        return new ArrayList(map.values());
    }

    public static void main(String[] args) {
        String[] strs = {"eat", "tea", "tan", "ate", "nat", "bat"};
        LeetCode_49_groupAnagrams leetCode_49_groupAnagrams = new LeetCode_49_groupAnagrams();
        List<List<String>> lists = leetCode_49_groupAnagrams.groupAnagrams(strs);
        System.out.println(lists);
    }
}
