package algorithm.leetcode;

public class LeetCode_171_titleToNumber {
    public static int titleToNumber(String columnTitle) {
        int res = 0;
        char[] columnTitleCharArrays = columnTitle.toCharArray();
        int length = columnTitleCharArrays.length;
        for (int i = 0; i < length; i++) {
            int num = columnTitleCharArrays[i] - 'A' + 1;
            res += num * Math.pow(26, length - i - 1);

        }

        return res;
    }

    public static void main(String[] args) {
        int res = titleToNumber("AB");
        System.out.println("res -> " + res);
    }
}
