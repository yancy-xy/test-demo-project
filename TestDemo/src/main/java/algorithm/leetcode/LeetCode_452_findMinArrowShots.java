package algorithm.leetcode;

import java.util.Arrays;

/***
 * 贪心->一支箭尽可能的射穿多个区间。
 */
public class LeetCode_452_findMinArrowShots {
    public int findMinArrowShots(int[][] points) {
        int res = 1;
        if (points.length < 1) return 0;
        //以右端点排序->思考 为何不用做端点排序
        Arrays.sort(points, (a, b) -> {
            return Integer.compare(a[1], b[1]);
        });
        int axis = points[0][1];
        for (int i = 1; i < points.length; i++) {
            //若当前区间的左端点大于前一个区间的右端点->这一只箭不能射穿当前区间
            if (axis < points[i][0]) {
                res += 1;
                axis = points[i][1];
            }
        }
        return res;
    }

    public static void main(String[] args) {

    }
}
