/*
package algorithm;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LeetCode_341_NestedIterator implements Iterator<Integer> {

    private List<Integer> list = new ArrayList<>();
    private int index;

    private void add(List<LeetCode_341_NestedIterator> nestedList) {
        for (LeetCode_341_NestedIterator nestedInteger : nestedList) {
            if (nestedInteger.isInteger()) {
                list.add(nestedInteger.getInteger());
            } else {
                add(nestedInteger.getList());
            }
        }
    }


    public LeetCode_341_NestedIterator(List<LeetCode_341_NestedIterator> nestedList) {
        add(nestedList);
    }

    @Override
    public Integer next() {
        return list.get(index++);
    }

    @Override
    public boolean hasNext() {
        return index < list.size();
    }
}
*/
