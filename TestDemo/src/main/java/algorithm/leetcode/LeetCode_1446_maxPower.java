package algorithm.leetcode;

public class LeetCode_1446_maxPower {
    public int maxPower(String s) {
        int res = 1;
        int temp = 1;
        for (int i = 1; i < s.length(); i++) {
            if (s.charAt(i) == s.charAt(i - 1)) {
                temp++;
                res = Math.max(res, temp);
            }
            temp = 1;
        }
        return res;
    }
}
