package algorithm.leetcode;

import java.util.ArrayList;
import java.util.List;

public class LeetCode_830_largeGroupPositions {
    public List<List<Integer>> largeGroupPositions(String s) {
//        s = s + "A";
        List<List<Integer>> result = new ArrayList<>();
        int begin = 0;
        for (int i = 1; i < s.length(); i++) {
            if (s.charAt(i) != s.charAt(i - 1)) {
                if (i - begin >= 3) {
                    List<Integer> temp = new ArrayList<>();
                    temp.add(begin);
                    temp.add(i - 1);
                    result.add(temp);
                }
                begin = i;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        LeetCode_830_largeGroupPositions leetCode_830_largeGroupPositions = new LeetCode_830_largeGroupPositions();
        String s = "aaa";
        List<List<Integer>> res = leetCode_830_largeGroupPositions.largeGroupPositions(s);
        System.out.println("list ->" + res);

    }
}
