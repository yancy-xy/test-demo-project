package algorithm.leetcode;

public class LeetCode_190_reverseBits {
    // you need treat n as an unsigned value
    public int reverseBits(int n) {
        return Integer.reverse(n);
    }

    public static void main(String[] args) {

    }
}
