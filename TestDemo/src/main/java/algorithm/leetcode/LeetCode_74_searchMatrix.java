package algorithm.leetcode;

public class LeetCode_74_searchMatrix {
    public boolean searchMatrix(int[][] matrix, int target) {
        int row = matrix.length;
        int col = matrix[0].length;
        int n = 0;
        for (int i = 0; i < row; i++) {
            if (matrix[i][0] <= target && target <= matrix[i][col - 1]) {
                n = i;
                break;
            }
        }
        //二分查找
        int left = 0, right = col - 1;

        while (left <= right) {
            int mid = (right + left) / 2;
            if (matrix[n][mid] == target) {
                return true;
            } else if (matrix[n][mid] < target) {
                left = mid + 1;
            } else if (matrix[n][mid] > target) {
                right = mid - 1;
            }
        }

        return false;
    }

    public static void main(String[] args) {

    }
}
