package algorithm.leetcode;

public class LeetCode_922_sortArrayByParityII {
    public int[] sortArrayByParityII(int[] A) {
        int[] res = new int[A.length];
        int oddIndex = 1;
        int evenIndex = 0;
        for (int item : A) {
            if (item % 2 == 0) {
                res[evenIndex] = item;
                evenIndex += 2;
            } else {
                res[oddIndex] = item;
                oddIndex += 2;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        int[] A = {1, 3, 2, 4, 6, 5};
        LeetCode_922_sortArrayByParityII leetCode_922_sortArrayByParityII = new LeetCode_922_sortArrayByParityII();
        for (int item : leetCode_922_sortArrayByParityII.sortArrayByParityII(A)) {
            System.out.print(item + ",");
        }
    }
}
