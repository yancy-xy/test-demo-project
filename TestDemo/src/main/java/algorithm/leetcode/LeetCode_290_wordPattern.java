package algorithm.leetcode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class LeetCode_290_wordPattern {
    public boolean wordPattern(String pattern, String s) {
        String[] words = s.split(" ");
        char[] patternCharArrays = pattern.toCharArray();
        Set<String> wordSet = new HashSet<>();
        if (patternCharArrays.length != words.length) return false;
        Map<Character, String> map = new HashMap<>();
        for (int i = 0; i < patternCharArrays.length; i++) {
            if (!map.containsKey(patternCharArrays[i])) {
                if (!wordSet.add(words[i])) {
                    return false;
                }
                map.put(patternCharArrays[i], words[i]);
            }
            if (!words[i].equals(map.get(patternCharArrays[i]))) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        LeetCode_290_wordPattern leetCode_290_wordPattern = new LeetCode_290_wordPattern();
        boolean flag = leetCode_290_wordPattern.wordPattern("abba", "dog dog dog dog");
        System.out.println(flag);
    }
}
