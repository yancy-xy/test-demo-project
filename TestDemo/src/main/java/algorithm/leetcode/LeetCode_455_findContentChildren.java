package algorithm.leetcode;

import java.util.Arrays;

public class LeetCode_455_findContentChildren {
    public int findContentChildren(int[] g, int[] s) {
        /*int child = 0, cookie = 0;
        Arrays.sort(g);//孩子排序*/
/*        Arrays.sort(s);//饼干排序
        while () {

        }*/

        /*int res = 0;
        Arrays.sort(g);//孩子的胃口
        Arrays.sort(s);//饼干的尺寸
        Deque<Integer> gDeque = new LinkedList<>();
        Deque<Integer> sDeque = new LinkedList<>();
        for (int item : g) {
            gDeque.addFirst(item);
        }
        for (int item : s) {
            sDeque.addFirst(item);
        }
        while (!sDeque.isEmpty() && !gDeque.isEmpty()) {
            int temp = gDeque.pop();
            if (sDeque.peek() >= temp) {
                res += 1;
                sDeque.removeFirst();
            } else {
                continue;
            }
        }
        return res;*/
        int child = 0, cookie = 0;
        Arrays.sort(g);
        Arrays.sort(s);
        while (child < g.length && cookie < s.length) {
            if (g[child] >= s[cookie]) {
                cookie += 1;
            }
            child += 1;
        }
        return child;
    }
}
