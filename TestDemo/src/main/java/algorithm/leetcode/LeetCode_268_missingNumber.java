package algorithm.leetcode;

import java.util.HashSet;
import java.util.Set;

public class LeetCode_268_missingNumber {
    //利用哈希表解决
    public int missingNumber(int[] nums) {
        int length = nums.length;
        Set<Integer> set = new HashSet<>();
        for (Integer item : nums) {
            set.add(item);
        }
        for (int i = 0; i <= length; i++) {
            if (!set.contains(i)) return i;
        }
        return 0;
    }
}
