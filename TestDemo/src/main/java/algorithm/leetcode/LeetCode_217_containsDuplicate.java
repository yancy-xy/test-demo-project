package algorithm.leetcode;

import java.util.HashSet;
import java.util.Set;

public class LeetCode_217_containsDuplicate {
    public boolean containsDuplicate(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < nums.length; i++) {
            set.add(nums[i]);
        }
        return nums.length != set.size();
    }

    public static void main(String[] args) {

    }
}
