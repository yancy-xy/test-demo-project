package algorithm.leetcode;

public class LeetCode_231_isPowerOfTwo {
    public static boolean isPowerOfTwo(int n) {
        if (n <= 0) return false;
        if ((n & (n - 1)) == 0) return true;
        return false;
    }

    public static void main(String[] args) {
        boolean res = isPowerOfTwo(15);
        System.out.println(res);
    }
}
