package algorithm.leetcode;

import java.util.Arrays;
import java.util.PriorityQueue;

public class LeetCode_973_kClosest {

    public int[][] kClosest(int[][] points, int K) {
        int[][] res = new int[K][2];
        PriorityQueue<int[]> queue = new PriorityQueue<>((int[] o1, int[] o2) -> {
            double v1 = Math.pow(o1[0], 2) + Math.pow(o1[1], 2);
            double v2 = Math.pow(o2[0], 2) + Math.pow(o2[1], 2);
            return v1 > v2 ? 1 : -1;
        });
        for (int i = 0; i < points.length; i++) {
            queue.add(points[i]);
        }
        int index = 0;
        while (index < K) {
            res[index] = queue.poll();
            index += 1;
        }
        return res;
    }

    public static void main(String[] args) {
        int[][] points = {{1, 3}, {-2, 2}};
        LeetCode_973_kClosest leetCode_973_kClosest = new LeetCode_973_kClosest();
        int[][] res = leetCode_973_kClosest.kClosest(points, 1);
        for (int[] item : Arrays.asList(res)) {
            System.out.println(item[0] + "," + item[1]);
        }
    }
}
