package algorithm.leetcode;

public class LeetCode_1004_longestOnes {
    public int longestOnes(int[] A, int K) {
        int left = 0, res = 0;
        for (int right = 0; right < A.length; right++) {
            if (A[right] == 0) {
                if (K == 0) {
                    while (A[left] == 1) left++;
                    left++;
                } else {
                    K--;
                }
            }
            res = Math.max(res, right - left + 1);
        }
        return res;
    }
    public static void main(String[] args) {

    }
}
