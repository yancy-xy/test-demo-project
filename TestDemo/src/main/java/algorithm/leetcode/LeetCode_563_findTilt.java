package algorithm.leetcode;

public class LeetCode_563_findTilt {
    //左右中  后序遍历
    //深度 = math.abs(left - right)
    //
    int result;

    public int findTilt(TreeNode root) {
        this.result = 0;
        dfs(root);
        return this.result;
    }

    private int dfs(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int left = dfs(root.left);
        int right = dfs(root.right);
        this.result += Math.abs(left - right);
        return left + right + root.val;
    }


    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}
