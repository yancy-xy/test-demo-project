package algorithm.leetcode;

import java.util.HashSet;
import java.util.Set;

public class LeetCode_349_intersection {
    class Solution {
        public int[] intersection(int[] nums1, int[] nums2) {
            Set<Integer> resSet = new HashSet<>();
            Set<Integer> nums1Set = new HashSet<>();
            for (Integer item : nums1) {
                nums1Set.add(item);
            }
            for (Integer item : nums2) {
                if (nums1Set.contains(item)) {
                    resSet.add(item);
                }
            }
            int[] res = new int[resSet.size()];
            
            Integer[] restemp = resSet.toArray(new Integer[resSet.size()]);

            for (int i = 0; i < restemp.length; i++) {
                res[i] = restemp[i];
            }

            /*Object[] ints = resSet.toArray();
            int[] res = new int[ints.length];
            for (int i = 0; i < ints.length; i++) {
                res[i] = (Integer) ints[i];
            }*/
            return res;
        }
    }
}
