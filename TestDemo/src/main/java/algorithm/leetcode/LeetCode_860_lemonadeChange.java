package algorithm.leetcode;

import java.util.Arrays;

public class LeetCode_860_lemonadeChange {
    public boolean lemonadeChange(int[] bills) {
        int[] map = new int[21];
        Arrays.fill(map, 0);
        for (int i = 0; i < bills.length; i++) {
            if (bills[i] == 5) {
                map[5] += 1;
            } else if (bills[i] == 10) {
                map[5] -= 1;
                map[10] += 1;
            } else if (map[10] > 0) {
                map[10] -= 1;
                map[5] -= 1;
                map[20] += 1;
            } else {
                map[5] -= 3;
                map[20] += 1;
            }
            if (map[5] < 0 || map[10] < 0) return false;
        }
        return true;
    }

    public static void main(String[] args) {

    }
}
