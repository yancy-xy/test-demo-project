package algorithm.leetcode;

public class LeetCode_746_minCostClimbingStairs {
    public int minCostClimbingStairs(int[] cost) {
        int length = cost.length;
        int[] dp = new int[length + 1];
        for (int i = 2; i < length + 1; i++) {
            dp[i] = Math.min(dp[i - 1] + cost[i - 1], dp[i - 2] + cost[i - 2]);
        }
        return dp[length];
    }

    public static void main(String[] args) {
        int[] cost = {1, 100, 1, 1, 1, 100, 1, 1, 100, 1};
        LeetCode_746_minCostClimbingStairs leetCode_746_minCostClimbingStairs = new LeetCode_746_minCostClimbingStairs();
        int res = leetCode_746_minCostClimbingStairs.minCostClimbingStairs(cost);
        System.out.println(res);
    }
}
