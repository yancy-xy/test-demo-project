package tdd;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class WordFileRead {

    /***
     * 读取文件内容
     * @param filePath
     * @return
     * @throws IOException
     */
    public String readFile(String filePath) throws IOException {
        StringBuilder sbr = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String str;
            while ((str = br.readLine()) != null) {
                sbr.append(str);
            }
        }
        return sbr.toString();
    }

    private char[] getStringCharArray(String inputString) {
        return inputString.toCharArray();
    }
}
