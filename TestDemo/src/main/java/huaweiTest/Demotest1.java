package huaweiTest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Demotest1 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int count = Integer.valueOf(br.readLine());//总的字符串数目
        List<String> resList = new ArrayList<>();
        for (int i = 0 ; i< count ; i++){
            String str = br.readLine().trim();
            char[] chars = str.toCharArray();
            char[] res = new char[chars.length];
            int [] dp = getIndexDp(chars.length);
            for(int j = 0 ; j < chars.length ; j++){
                res[j] = getChar(chars[j] , dp[j]);
            }
            resList.add(String.valueOf(res));
        }

        for (String str : resList){
            System.out.println(str);
        }

    }
    public static char getChar(char c, int index){
        char res = (char) (c + index%26 );
        while( res > 'z'){
            res = (char) (res -'z' +'a' -1);
        }
        return  res;
    }
    public static int[] getIndexDp(int index){
        int [] dp = new int[index+1];
        dp[0] = 1;
        if(index == 0) return dp;
        dp[1] = 2;
        if(index == 1) return dp;
        dp[2] = 4;
        if(index == 2) return dp;
        for (int i = 3 ; i<= index ; i++){
            dp[i] = dp[i-1] + dp[i-2] +dp[i-3];
        }
        return dp;
    }

}
