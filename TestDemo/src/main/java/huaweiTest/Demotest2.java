package huaweiTest;

import java.util.Scanner;


//输入正整数n  求k个连续的正整数和为n的序列。
public class Demotest2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int totalNum = sc.nextInt();//数字和
        int count = sc.nextInt();//数字个数
        System.out.println(getNum(totalNum , count));
    }
    public static String getNum(int totalNum, int count){
        int midNum = totalNum / count ;
        if(midNum <1) return "-1";
        if(midNum - count/2 <0) return "-1";
        StringBuffer  sb = new StringBuffer();
        for(int i = 1 ; i<=count ; i++){
            sb.append(midNum - count/2+ i +" ");
        }
        if(sb.length() != 0) return sb.toString().trim();
        else return "-1";
    }
}
