package huaweiTest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Demotest3 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int maxNum = Integer.valueOf(br.readLine());//单个面试官 面试的人数
        int totalNum = Integer.valueOf(br.readLine());//总共的面试场数；
        int count = 0;
        List<String> timeList = new ArrayList<>();
        String str;
        int maxTime =0;
        while ( (str = br.readLine()) != null){
            timeList.add(str);
            maxTime = Math.max(maxTime , Integer.valueOf(str.split(" ")[1]));
        }


    }
    public int[] getTimeTable(List<String> timeList ,int maxTime){
        int[]res = new int[maxTime+1];
        for(String str : timeList){
            int start = Integer.valueOf(str.split(" ")[0]);
            int end = Integer.valueOf(str.split(" ")[1]);
            int ptr =start;
            while(ptr < end){
                res[ptr] +=1;
            }
        }
        return res;
    }
}
