package designModel.templateModel;

public class templateClientTest {
    public static void main(String[] args) {
        HouseTemplate houseOne = new HouseOne("房子1", false);
        HouseTemplate houseTwo = new HouseTwo("房子2");
        houseOne.buildHouse();
        System.out.println("-------------------------------");
        houseTwo.buildHouse();
    }
}
