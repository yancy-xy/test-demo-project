package designModel.stategyModel;

/**
 * 定义一个环境类（Contex），类中持有一个对公共接口的引用，以及相应的get、set方法、构造方法
 */
public class Context {
    Weapon weapon;

    public Context(Weapon weapon) {
        this.weapon = weapon;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public void gun() {
        weapon.gun();
    }
}
