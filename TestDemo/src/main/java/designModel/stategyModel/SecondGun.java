package designModel.stategyModel;
/**
 * 具体策略实现-策略2
 */
public class SecondGun implements Weapon {
    @Override
    public void gun() {
        System.out.println("使用狙击枪");
    }
}
