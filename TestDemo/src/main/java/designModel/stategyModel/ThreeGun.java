package designModel.stategyModel;

/**
 * 具体策略实现-策略3
 */
public class ThreeGun implements Weapon {
    @Override
    public void gun() {
        System.out.println("使用霰弹枪");
    }
}
