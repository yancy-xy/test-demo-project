package designModel.stategyModel;

/**
 * 具体策略实现-策略1
 */
public class FirstGun implements Weapon {
    @Override
    public void gun() {
        System.out.println("使用手枪");
    }
}
