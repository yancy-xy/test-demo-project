package designModel.stategyModel;

/**
 * 给策略对象（枪）定义一个公共接口
 */
public interface Weapon {
    public void gun();
}
