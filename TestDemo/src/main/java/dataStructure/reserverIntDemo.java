package dataStructure;

public class reserverIntDemo {
    public static void main(String[] args) {
        Solution demo = new Solution();
        System.out.println(demo.reverse(123));
    }
    static class Solution {
        public int reverse(int x) {
            char[] numChars = String.valueOf(x).toCharArray();
            int res =0;
            for(int i = numChars.length-1  ; i>0 ; i--){
                res = res *10 + (numChars[i] -'0');
            }

            if(x <0 ) return -res;
            else return res*10 +numChars[0] - '0';

        }
    }
}
