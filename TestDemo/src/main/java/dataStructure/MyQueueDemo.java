package dataStructure;

import java.util.Deque;
import java.util.LinkedList;

public class MyQueueDemo {

    static class MyQueue {
        Deque<Integer> stack1;
        Deque<Integer> stack2;
        /** Initialize your data structure here. */
        public MyQueue() {
            stack1 = new LinkedList<>();
            stack2 = new LinkedList<>();
        }

        /** Push element x to the back of queue. */
        public void push(int x) {
            if(!stack1.isEmpty() && stack2.isEmpty()){
                while(!stack1.isEmpty()){
                    stack2.addFirst(stack1.removeFirst());
                }
            }
            if(stack1.isEmpty()){
                while(!stack2.isEmpty()){
                    stack1.addFirst(stack2.removeFirst());
                }
                stack1.addFirst(x);
            }
        }

        /** Removes the element from in front of queue and returns that element. */
        public int pop() {
            return stack1.removeFirst();
        }

        /** Get the front element. */
        public int peek() {
            return stack1.peek();
        }

        /** Returns whether the queue is empty. */
        public boolean empty() {
            return stack1.isEmpty();
        }
    }

    public static void main(String[] args) {
        MyQueue quque = new MyQueue();
        quque.push(1);
        quque.push(2);
        quque.peek();
        quque.pop();
        quque.empty();
    }
}
