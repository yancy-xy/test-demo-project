package dataStructure;

class demo8 {
    public static String longestCommonPrefix(String[] strs) {
        if (strs.length == 0) return "";
        //Arrays.sort(strs);
        String prefix = strs[0];
        for (int i = 0; i < strs.length; i++) {
            int index = 0;
            while ((index < prefix.length())
                    && (index < strs[i].length()
                    && (prefix.charAt(index) == strs[i].charAt(index)))) {
                index += 1;
            }
            prefix = prefix.substring(0, index);
        }
        return prefix;
    }

    public static void main(String[] args) {
        System.out.println(longestCommonPrefix(new String[]{"dog", "racecar", "car"}));
    }
}
