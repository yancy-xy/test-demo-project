package dataStructure;

import java.util.Scanner;

//多线程实现交替打印abcd 打印次数为系统输入次数
public class demo6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int count = sc.nextInt();
        printDemo demo = new printDemo(count);
        new Thread(() -> {
            try {
                demo.print("a", 1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(() -> {
            try {
                demo.print("b", 2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(() -> {
            try {
                demo.print("c", 3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(() -> {
            try {
                demo.print("d", 0);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

    static class printDemo {
        volatile int count = 0;
        int num;

        printDemo(int n) {
            this.num = n;
        }

        synchronized void print(String str, int flag) throws InterruptedException {//输入打印的字符  与自己的标识位
            for (int i = 0; i < num; i++) {
                while (count % 4 != flag) {
                    this.wait();
                }
                System.out.print(str);
                count += 1;
                this.notifyAll();
            }
        }
    }
}
