package dataStructure;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
/*将一个英文语句以单词为单位逆序排放。例如“I am a boy”，逆序排放后为“boy a am I”
        所有单词之间用一个空格隔开，语句中除了英文字母外，不再包含其他字符


        接口说明

*//**
 * 反转句子
 *
 * @param sentence 原句子
 * @return 反转后的句子
 *//*
public String reverse(String sentence);*/
public class demo1 {
    public static void main(String[] args) throws IOException {
        //Scanner sc = new Scanner(System.in);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str;
        while ((str = br.readLine())!=null){
            System.out.println(getStr(str));
        }
    }
    public static String getStr(String str){
        String[] words = str.split(" ");
        StringBuffer sb = new StringBuffer();
        for(int i = words.length -1 ; i>=0 ; i-- ){
            sb.append(words[i]).append(" ");
        }
        return sb.toString().trim();
    }
}
