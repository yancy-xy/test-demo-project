package dataStructure;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/*题目描述
        定义一个二维数组N*M（其中2<=N<=10;2<=M<=10），如5 × 5数组下所示：
        输入描述:
        输入两个整数，分别表示二位数组的行数，列数。再输入相应的数组，其中的1表示墙壁，0表示可以走的路。数据保证有唯一解,不考虑有多解的情况，即迷宫只有一条通道。

        输出描述:
        左上角到右下角的最短路径，格式如样例所示。

        示例1
        输入
        复制
        5 5
        0 1 0 0 0
        0 1 0 1 0
        0 0 0 0 0
        0 1 1 1 0
        0 0 0 1 0
        输出
        复制
        (0,0)
        (1,0)
        (2,0)
        (2,1)
        (2,2)
        (2,3)
        (2,4)
        (3,4)
        (4,4)*/
public class demo5 {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str = br.readLine();
        Map<Integer , Integer> resMap = new TreeMap<>();//设置解的值对
        String[] rowAndColumn = str.split(" ");
        Deque queue = new LinkedList<>();
        int row = Integer.valueOf(rowAndColumn[0]) ,col = Integer.valueOf(rowAndColumn[1]);
        /*while ( (str = br.readLine()) != null){
            //0, 1, 0, 0, 0,
            String [] rowNums = str.split(",");
            if (rowNums.length == col) {
                for (int i = 0; i < M; i++) {
                    maze[row][i] = Integer.parseInt(inputs[i]);
                }
            }
        }
        findShortestPath(maze);*/
    }
    /*public static boolean isDown (int[][] maze , int[][] path){

    }*/
}
