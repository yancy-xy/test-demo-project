package dataStructure;

import java.io.*;
import java.nio.Buffer;
import java.util.*;
public class test {

    public static void main(String[] args) throws IOException {
        PriorityQueue p= new PriorityQueue();
        PriorityQueue<Integer> p1 = new PriorityQueue<Integer>((o1, o2) -> {return o2 - o1;});


        /*Scanner scan=new Scanner(System.in);
        String mask=scan.nextLine();
        String ip1=scan.nextLine();
        String ip2=scan.nextLine();
        System.out.println(checkNetSegment(mask , ip1 , ip2));*/

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str ;
        while ((str = br.readLine()) != null) {
            System.out.println(str);
        }

    }
    public static int checkNetSegment(String mask, String ip1, String ip2)
    {
        /*在这里实现功能*/
        if(mask.isEmpty() || ip1.isEmpty()  || ip2.isEmpty() ) return 1;
        String[] maskInt = mask.split("\\.");
        String[] ip1Int = ip1.split("\\.");
        String[] ip2Int = ip2.split("\\.");
        if(maskInt.length!= 4 || ip1Int.length != 4 || ip2Int.length != 4) return 1;
        for(int i = 0 ; i<4 ; i++){
            Integer ip1AndAddress = Integer.valueOf(maskInt[i]) & Integer.valueOf(ip1Int[i]);
            Integer ip2AndAddress = Integer.valueOf(maskInt[i]) & Integer.valueOf(ip2Int[i]);
            if(ip1AndAddress != ip2AndAddress) return 2;
        }
        return 0;
    }

}