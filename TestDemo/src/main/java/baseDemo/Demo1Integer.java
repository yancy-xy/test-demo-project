package baseDemo;

public class Demo1Integer {
    public static void main(String[] args) {
        Integer t1 = new Integer(1);
        Integer t2 = new Integer(1);
        Integer t3 = 127;
        Integer t4 = 127;
        Integer t5 = 1;
        Integer t6 = 1;
        Integer t7 = 128;
        Integer t8 = 128;
        System.out.println(t1 == t2);
        System.out.println(t1 == t5);
        System.out.println(t4 == t3);
        System.out.println(t5 == t6);
        System.out.println(t7 == t8);
    }
}
