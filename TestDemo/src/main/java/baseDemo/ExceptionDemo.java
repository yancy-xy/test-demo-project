package baseDemo;

public class ExceptionDemo {
    public static String output="";
    public static void foo(int i){
        try{
            if(i == 1){
                throw new Exception();
            }
            output +="1";
        }catch (Exception e){
            output +="2";
            System.out.println("output = "+ output);
            return;
        }finally {
            output +="3";
        }
        output +="4";
        System.out.println("output = "+ output);
    }

    public static void main(String[] args) {
        foo(0);
        foo(1);
        int i= 0 ;
        System.out.println( i );
    }
}
