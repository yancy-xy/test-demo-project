package baseDemo;

import java.util.ArrayList;
import java.util.List;

public class ArrayListDemo {
    public static void main(String[] args) {
        /*ArrayList<Integer> list = new ArrayList(10);
        System.out.println(list.size());
        //list.remove(10);
        for (int i = 0; i < 12; i++) {
            list.add(i);
            System.out.println(list.size());
        }
        list.ensureCapacity(8);
        Object[] array = list.toArray();
        System.out.println(array.length);
        list.set(5, 10);*/

        List<String> listA = new ArrayList<>();
        listA.add("aaa");
        listA.add("bbb");
        listA.add("ccc");
        List<String> listB = new ArrayList<>();
        listB.add("bbb");
        //取a-b的集合
        listA.removeAll(listB);
        System.out.println("取a-b的集合");
        System.out.println(listA);
        System.out.println(listB);
        //取a b的交集
        System.out.println("取a b的交集");
        listA.addAll(listB);
        listA.retainAll(listB);
        System.out.println(listA);
        System.out.println(listB);

    }
}
