package baseDemo;

public class StringDemo {
    public static void main(String[] args) {
        String str1 = new String("j") + new String("ava123");
        str1.intern();
        String str2 = "java123";
        System.out.println(str1 == str2);

        String str3 = new String("ja") + new String("va2");
        String str4 = "java2";
        str3.intern();
        System.out.println(str3 == str4);

        System.out.println("----------------------");

        String a = new String("ab");
        String b = new String("ab");
        String c = "ab";
        String d = "a";
        String e = new String("b");
        String f = d + e;
        String g = "b";
        String h = d + g;
        System.out.println(a == c);
        System.out.println(a.intern() == b);
        System.out.println(a.intern() == b.intern());
        System.out.println(a.intern() == c);
        System.out.println(a.intern() == f);
        System.out.println(c == h);
    }
}
