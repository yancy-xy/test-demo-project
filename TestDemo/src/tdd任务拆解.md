1.读取文件内容并展现                    完成
2.根据文件内容,收集文件单词             
3.计算文件单词频率
4.根据单词频率排序
5.打印内容



测试案例设计:
1.文件内容为: the day is sunny the the the sunny is is 
2.文件中的单词为: the day is sunny
3.文件中单词的频率为: the 4 ; day 1 ; is 3 ; sunny 2 
4.降序排序后为: the 4 ; is 3 ; sunny 2 ; day 1
5.打印出来的内容为: the 4 ; is 3 ; sunny 2 ; day 1