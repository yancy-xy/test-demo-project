package redission;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

public class LockTest {

    private static Map<String,String> parms = new HashMap<String,String>();

    public static void main(String[] args) {
        testCountDownLatch();
    }

    public static void testCountDownLatch(){

        int threadCount = 2000;

        final CountDownLatch latch = new CountDownLatch(threadCount);
        for(int i=0; i< threadCount; i++){

            new Thread(new Runnable() {

                @Override
                public void run() {

                    System.out.println("线程" + Thread.currentThread().getId() + "开始出发");

                    try {
                        parms.put("sid", "线程id=" +Thread.currentThread().getId());
                        String result = HttpClientUtil.doGet("http://127.0.0.1:8080/demo/lock", parms);
                        System.out.println("线程result" + result);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    System.out.println("线程" + Thread.currentThread().getId() + "已到达终点");

                    latch.countDown();
                }
            }).start();
        }

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(threadCount+"个线程已经执行完毕");

    }
}
