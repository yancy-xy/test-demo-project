package tdd;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.Method;

public class wordFileReadTest {
    private static final Logger logger = LoggerFactory.getLogger(wordFileReadTest.class);


    @Test
    public void readFile_Test() {
        String content = "";
        String fileStr = "the day is sunny the the the sunny is is";
        try {
            WordFileRead wordFileRead = new WordFileRead();
            String filePath = "C:\\Users\\39103\\Desktop\\demo.txt";
            content = wordFileRead.readFile(filePath);
        } catch (IOException e) {
            logger.error("error -> e:", e);
        }
        logger.info("content -> {}", content);
        Assert.assertEquals(fileStr, content);
    }

    public void splitWord_Test() {
        /*String[] expectWords = {};
        wordFileRead wordFileRead = new wordFileRead();
        String[] words = wordFileRead.splitWords();
        logger.info("split words is {}", words);
        Assert.assertEquals(words, );*/
    }

    @Test
    public void getStringCharArray_test() {
        try {
            WordFileRead wordFileRead = WordFileRead.class.newInstance();
            Method getStringCharArrayMethod = wordFileRead.getClass().getDeclaredMethod("getStringCharArray", String.class);
            getStringCharArrayMethod.setAccessible(true);
            String input = "abcdefg";
            char[] res = (char[]) getStringCharArrayMethod.invoke(wordFileRead, input);
            System.out.println(res);
        } catch (Exception e) {
            logger.error("exception , e", e);
        }
    }
}
