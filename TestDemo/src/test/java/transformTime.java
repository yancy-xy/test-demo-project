import java.text.DecimalFormat;

public class transformTime {
    public String util(long time) {
        DecimalFormat df = new DecimalFormat("#.00");
        String timeString = null;
        if (time == 0) {
            timeString = "0.00 ms";
        } else if (time < 1000) {
            timeString = df.format((double) time) + " ms";
        } else if (time < 60 * 1000) {
            timeString = df.format((double) time / 1000) + " ms";
        }
        return timeString;
    }

    public static void main(String[] args) {
        transformTime transformTime = new transformTime();
        String res = transformTime.util(0);
        System.out.println(res);
    }
}
